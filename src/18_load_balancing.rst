.. -*- mode: rst -*-

.. All these works are translation of "CouchDB: The Definitive Guide".
.. highlight:: javascript

.. 17. Load Balancing
   http://books.couchdb.org/relax/reference/scaling-basics

18. 負荷分散
------------------

.. Jill wakes up at 4:30 am looking dazzled at her mobile phone. She receives text message after text message one every minute. Finally, Joe calls. Joe is furious and Jill has trouble understanding what Joe is saying. In fact, Jill has a hard time remembering why Joe would call her in the middle of the night. Then she remembers: “Joe is running this online shop selling sports gear on one of your servers and he is furious because the server went down and Joe’s customers in New Zeeland [Sanity check time zones] are angry because they can’t get to the online shop.”

ジルは朝の４時半にケータイに起こされた。一分ごとにメールが来て、鳴っている。\
最後には電話がかかってきた。ジョーからだ。ジョーは怒り狂っていて何を言っているのかよく分からない。\
なんでこんな夜中にジョーが電話をかけてくるのか、ジルは理解するのに少し時間がかかったが、ようやく思い出した：\
ジョーはあなたのサーバでスポーツ用品のオンラインショップを開いていて、あなたのサーバが落ちていた。\
…ジョーのニュージーランド（時差は大丈夫？）の顧客がサイトを見れず怒っているので、ジョーは激怒していたのです。

.. This is a typical scenario and you probably have seen a lot of variations of it being either in the role of Jill, or Joe, or both. If you are Jill you want to sleep at night and if you are Joe you want your customers to buy from you whenever it pleases them.

これはあなたがジルもしくはジョーの役で何度も出くわした典型的なシナリオだと思います。\
もしもジルなら夜は寝ていたいし、もしもジョーなら顧客がほしいときに商品を買ってもらいたいでしょう。

.. Having a Backup

バックアップしておく
~~~~~~~~~~~~~~~~~~~~~~~~

.. The problems persists, computers fail and there are a lot of ways they can fail. There are hardware problems, power outages, bugs in the operating system or application software. Only CouchDB doesn’t have any bugs. Wait, that is of course not true, there can even be problems in CouchDB, no piece of software is free of bugs (except maybe Donald Knuth’s TeX system).

問題は、コンピュータは壊れること、しかも何通りもの方法で壊れることです。\
ハードウェアの問題、停電、OSやアプリケーションのバグなど。CouchDBだけがバグを持っていません。\
いや、もちろん冗談ですが、CouchDBでさえ問題があって、どんなソフトウェアの欠片でさえバグからは逃れられません（ただしクヌースのTeXは例外でしょう）。

.. Whatever the cause is, you want to make sure that the service you are providing (in this case the database for an online store) is resilient against failure. The road to resilience is a road of finding and removing single points of failure: A server’s power supply can fail. To avoid the server turning off on such an event, most come with at least two power supplies. To take this further, you could get a server where everything exists twice or more often, but that would be a highly specialized (and expensive) piece of hardware. It is much cheaper to get two similar servers where the one can take over when the other has a problem. You need to make sure both servers have the same set of data in order to switch them without a user noticing.

故障の原因が何であれ、あなたのサービス（このケースではオンラインストアのデータベース）が\
故障せずにちゃんと動いていることを確信したいと思っていることでしょう。耐障害性への道は、\
SPOF（単一故障点）を発見して取り除く道です：例えば給電は止まることがあります。\
サーバがそのようなイベントを避けるために、電源を冗長化します。これが行き過ぎると、\
サーバの全てを二重化（もしくはそれ以上の多重化）をしたくなってきますが、\
高度に特殊な（しかも高価な）ハードウェアになってしまいます。似たようなサーバを二つ\
並べて、一方に問題があったら他方が交代するように作る方がずっと安上がりです。\
ユーザに気付かれずに切り替えるために、どちらのサーバも同じデータを持っていなければなりません。

.. Removing all single points of failure will give you a highly available or fault tolerant system. The order of tolerance is only restrained by your budget. If you can’t afford to lose a customer’s shopping cart in any event, you need to store it on at least two servers in at least to far apart geographical locations.

全てのSPOF(単一故障点)を取り除けば、可用性、耐障害性の高いシステムが作れます。\
耐障害性の優先順位はあなたの予算のみに依存します。もしも顧客のショッピングカートが\
どんな状況でも失われてはならないのであれば、少なくとも２台の遠隔地に分散したサーバに\
データを配置する必要があります。

.. Note::
   例えば、アマゾンは amazon.com のサイトでそれをやっています。もし、あるデータセンターが地震の犠牲になってもユーザーは買い物を続けられます。アマゾンの問題はあなたの問題とは違うかもしれませんが、代わりにあなたはデータセンターが見えなくなったときの全く別の問題を抱えることになるでしょう。それでも、サーバの故障くらいには対応したいと思うでしょう。

..  Amazon does that for example for their amazon.com Web site. If one datacenter is victim of an earthquake, a user will still be able to shop.
   It is likely though that Amazon’s problems are not your problems and that you have a whole set of new problems when your data center goes away. But you still want to be able to live through a server failure.

.. Before we dive into setting up a highly available CouchDB system, we look at another situation:

高可用なCouchDBシステムのセットアップに入る前に、別のシチュエーションを見ておきましょう：

.. Joe calls Jill during regular business hours and relays his customer’s complaints that loading the online shop takes “forever”. Jill takes a quick look at the server and concludes that this is a lucky problem to have, leaving Joe puzzled. Jill explains that Joe’s shop is suddenly attracting a lot more users that buy things. Joe chimes in “I got this great review on that blog”, that’s where they must come from and a quick referrer check reveals that indeed a lot of the new customers are coming from a single site. The blog post already includes comments of unhappy customers voicing their frustration with the slow site. Joe wants to make his customers happy and asks Jill what to do. Jill advises to set up a second server that can take half of the load of the current server, making sure all requests get answered in a reasonable amount of time. Joe agrees and Jill sets out to set things up.

ジョーは営業時間帯にジルに電話して、「オンラインショップのページ読み込みが **終わらない** 」という彼の顧客のクレームを伝えます。\
ジルはサーバをちょっと覗いてこれはラッキーな問題だと分かり、ジョーを混乱させます。ジョーの店は客が急激に増えているのが原因でした。\
ジョーは「あのブログで取り上げられたからだ」と気付き、そこからアクセスが来てるはずだと慌ててリファラをチェックすると、\
沢山の新規顧客がひとつのサイトから来ているのが分かります。問題のブログ記事には、
サイトが重いと既にネガティブなコメントがつけられていました。\
ジョーは顧客を満足させるために、ジルにどうしたらいいかを尋ねます。\
ジルは二台目のサーバを立てて、今のサーバの負荷を分散させれば、\
今のリクエストは捌けるだろうと答えました。ジョーはそれに合意して、ジルに仕事を頼みました。

.. The solution to the outlined problem looks a lot like the one for providing a fault tolerant setup: Install a second server, synchronize all data. The difference is that with fault tolerance, the second server just sits there and waits for the first one to fail. The the second case, a second server helps to answer all incoming requests. The second case is not fault tolerant. If one server crashes, the other would get all the requests and is likely to break down or at least provide very slow service, both of which is not acceptable. Keep in mind that while the solutions look similar, high availability and fault tolerance are not the same. We get back to the second scenario in a bit, but first we will have a look at how to set up a fault tolerant CouchDB system.

この例題の答えは、耐障害性の高いシステム構成を組むときの問題に似ています：\
二台目のサーバを立てて全てのデータを同期すればよいのです。耐障害性の場合は、\
二台目のサーバを立てても、二台目のサーバは一台目が故障するまでじーっと待っている点です。\
今回は、二台目のサーバはやってくるリクエストを捌きますが、耐障害性は低いです。\
サーバがひとつ故障すれば、もう片方が全てのリクエストを受け付けることになり、\
サービスがとても重くなるかサイアクサービスが落ちたりします（どちらもあり得ません）。\
問題に対する構成はどちらもよく似ていますが、高可用性と耐障害性は違うものだということを\
覚えておいてください。二つ目のシナリオに少し戻りますが、最初のシナリオでも耐障害性のあるCouchDBシステムを構築する方法を見ます。

.. We already gave it away in the previous chapters: The solution to synchronizing servers is replication.

とはいえ、前の章でそれは見ましたね： サーバを同期するには :ref:`レプリケーション <15_replication>` します。

`Copyright 2009 <http://creativecommons.org/licenses/by/3.0/>`_ , J. Chris Anderson, Jan Lehnardt & Noah Slater

Translated by UENISHI Kota
