.. -*- mode: rst -*-

.. CouchDB the Definitive Guide Translation to Japanese documentation master file, created by
   sphinx-quickstart on Wed Oct 28 09:07:05 2009.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

CouchDB: the Definitive Guide 日本語訳
========================================

This is a third-party Japanese translation of `"CouchDB: the Definitive Guide" <http://guide.couchdb.org/>`_ , which has been translated under the license term of CC-3.0. If you have any commentary or question feel free to contact `CouchDB_JP <http://groups.google.co.jp/group/couchdb-jp>`_ .

この翻訳は、Web上で公開されている書籍 `"CouchDB: the Definitive Guide" <http://guide.couchdb.org/>`_ を翻訳したものです。ライセンスは、原文に従って `Creative Commons - Attribution 3.0 Unported <http://creativecommons.org/licenses/by/3.0/>`_ としています。また、翻訳に対するコメントをお待ちしています。翻訳に対するコメントは、 `Bitbucketのページ <http://bitbucket.org/kuenishi/couchdb_definitive_guide_l10n_ja/issues/>`_ か、 `CouchDB_JP <http://groups.google.co.jp/group/couchdb-jp>`_ までお気軽にどうぞ。
`原著はすでに発売されています <http://www.amazon.co.jp/CouchDB-Definitive-J-Chris-Anderson/dp/0596155891>`_ 。

.. translated document starts - http://guide.couchdb.org/editions/1/en/index.html

.. Table of Contents

目次
=======

.. toctree::
   :maxdepth: 1

   foreword
   preface

.. Part I. Introduction

第1部 イントロダクション
---------------------------

.. toctree::
   :maxdepth: 1

   01_why_couchdb.rst
   02_eventual_consistency.rst
   03_getting_started.rst
   04_core_api.rst

.. Part II. Developing with CouchDB

第2部 CouchDBを使った開発
----------------------------------

.. toctree::
   :maxdepth: 1

   05_design_documents.rst
   06_finding_you_data.rst
   07_validation_functions.rst
   08_show_functions.rst
   09_transforming_views.rst

.. Part III. Example Application

第3部 アプリケーション構築例
------------------------------

.. toctree:: 
   :maxdepth: 1

   10_standalone_applications.rst
   11_managing_design_documents.rst
   12_storing_blog_posts.rst
   13_displaying_blog_posts.rst
   14_viewing_lists.rst

第4部 CouchDBのデプロイ
-------------------------

.. toctree::
   :maxdepth: 1
   
   15_scaling_basics.rst
   16_replication.rst
   17_conflict_management.rst
   18_load_balancing.rst
   19_clustering.rst

.. Part VI. Reference

第6部 参考情報
--------------------------

.. toctree::
   :maxdepth: 1

   20_change_notifications.rst
   21_views.rst
   22_security.rst
   23_high_performance.rst
   24_recipes.rst

.. Part VI. Appendix

第6部 付録
------------------

.. toctree::
   :maxdepth: 1

   appendix.rst
   4_G_power_of_btrees.rst

.. appendix.rstだけはバラす必要が今のところない。理由は、もしかしたら訳さないかもしれないから。

.. * :ref:`genindex`
.. * :ref:`modindex`
.. * :ref:`search`

.. rubric:: 訳注

.. [#f1] 英語のメーリングリストです。
.. [#f2] 翻訳版には残念ながらついていません。代わりに `CouchDB JP <http://groups.google.co.jp/group/couchdb-jp>`_ までどうぞ。

`Copyright 2009 <http://creativecommons.org/licenses/by/3.0/>`_ , J. Chris Anderson, Jan Lehnardt & Noah Slater

Translated by z.ohnami, Yohei Sasaki, fujishin, UENISHI Kota

