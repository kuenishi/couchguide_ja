.. -*- mode: rst -*-

.. highlight:: javascript

.. 11. Managing Design Documents

11.デザインドキュメントの管理
================================

.. Applications that live in CouchDB — nice. You just attach a bunch of HTML and JavaScript files to a design document and you are good to go. Spice that up with view-powered queries, and show functions that render any media type from your JSON documents and you have all it takes to write self-contained CouchDB applications.

CouchDBで動くアプリケーション - とてもクールです。HTMLとJavaScriptをデザインドキュメントへ格納すれば、すぐにアプリケーションを動かすことができます。viewによる問い合わせやJSON形式のドキュメントを様々な書式にレンダリングしてくれるshow関数など、アプリケーションを作る上で便利で魅力的な機能があり、 CouchDB内でデータからアプリケーションまでの全てを完結させることができるのです。

.. Working with the Example Application

サンプルアプリケーションを動かす
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. If you want to install and hack on your own version of Sofa while you read the following chapters, we’ll be using CouchApp to upload the source code as we explore it.

.. Note::
   この章を読みながらSofaをインストールして、ハックするのもよいでしょう。その場合は私たちがソースコードをアップロードするのにCouchAppを使用していることを抑えておくべきです。

   CouchDBへアプリケーションをデプロイして、データとソースコードを一緒に格納するやり方はより多くの人たちがWebアプリケーション作成に興味を持ち、個人用途のアプリケーションを開発する契機となるでしょう。そして、アプリケーションを一緒に共有し合って開発する際には、容易にスケールできる CouchDBのインフラが役に立つはずです。

.. We’re particularly excited by the prospect of deploying applications to CouchDB, because depending on a least-common denominator environment, that encourages users to control not just the data but also the source code, will let more people build personal web apps. And when the web app you’ve hacked together in your spare time hits the big-time, the ability of CouchDB to scale to larger infrastructure sure doesn’t hurt.


.. In a CouchDB design doc there are a mix of development languages (HTML, JS, CSS) that go into different places like attachments and design document attributes. Ideally you want your development environment to help you as much as possible. More importantly, you’re used to: Proper syntax highlighting, validation, integrated documentation, macros, helpers and whatnot. Editing HTML and JavaScript code as the string-attributes of a JSON object is not exactly modern computing.

CouchDBのデザインドキュメントでは添付ファイルやデザインドキュメントの属性など様々な場所に様々な開発言語(HTML,JS,CSSなど)が混在する状態になります。おそらく、開発を有利に進めるための開発環境が必要になるでしょう。さらには、構文チェックやバリデーション機能、ドキュメンテーション機能、マクロなどの多くの開発を支援する機能も必要になるでしょう。HTMLやJavaScriptをテキストとして編集するのはモダンコンピューティングとはいえません。

.. Lucky for you, we’ve been working on a solution: Enter CouchApp. CouchApp lets you develop CouchDB applications in a convenient directory hierarchy: Views and shows are separate .js-files neatly organized, your static assets (CSS, images) have their place and with the simplicity of acouchapp push you save your app to a design doc in CouchDB. Make a change? couchapp push and off you go.This chapter guides you through the installation and moving parts of CouchApp. You will learn what other neat helpers it has in store to make your life easier (Gosh, aren’t we awfully nice?). Once we have CouchApp, we’ll use it to install and deploy Sofa to a CouchDB database.

CouchAppという便利な開発ツールがあります。CouchAPPを使うと、viewやshowをそれぞれひとつのjsファイルで階層的に管理することができ、静的なリソース(CSSや画像など)も含めてpushコマンドひとつで簡単にデザインドキュメントとして登録することができるようになります。変更が入ったら、その都度pushコマンドを実行すればすぐに変更が反映されます。この章ではCouchappのインストールと操作を紹介しようと思います。CouchAppを使えるようになれば、Sofaのデプロイも簡単に実行できるようになります。

.. Installing CouchApp

CouchAppのインストール
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. The CouchApp Python script and JavaScript framework we’ll be using grew out of the work designing this example application. It’s now in use for a variety of applications, and has a mailing list, wiki, and a community of hackers. Just search the internet for "couchapp" to find the latest information. Many thanks to Benoît Chesneau for building and maintaining the library (and contributing to CouchDB’s Erlang codebase and many of the Python libraries.)
.. CouchApp is easiest to install using the Python easy_install script, which is part of the setuptoolspackage. If you are on the Mac, easy_install should already be available. If easy_install is not installed and you are on a Debian variant, such as Ubuntu, you can use the following command to install it:

CouchAppはPythonとJavaScriptのフレームワークで作られています。この作成パターンはメーリングリストや、wiki,ハッカーのコミュニティなど、色々なところで見かけると思います。インターネットでCouchAppに関する最新の情報を探してみてください。CouchAppの構築と保守についてはBenoît Chesneauさんに大きく貢献していただきました(CouchDBのErlangのコードやPythonのライブラリについてもご協力いただいています)。CouchAppはPythonのsetuptoolsパッケージにあるeasy_installを使えば、簡単にインストールすることができます。Macであれば、すでにeasy_installは利用可能な状態になっているでしょう。UbuntuのようなDebian系のディストリビューションで、easy_installがないようであれば、次のコマンドを実行してください。

::

   sudo apt-get install python-setuptools

.. If all goes well, you should see output like the following:

うまくいけば、以下のような結果が表示されます。

::

   Reading package lists... Done
   Building dependency tree Reading state information... 
   Done The following NEW packages will be installed:
     python-setuptools
   0 upgraded, 1 newly installed, 0 to remove and 53 not upgraded.
   Need to get 195kB of archives.
   After this operation, 909kB of additional disk space will be used.
   Get:1 http://us.archive.ubuntu.com karmic/main python-setuptools 0.6c9-0ubuntu4 [195kB]
   Fetched 195kB in 1s (108kB/s)
   Selecting previously deselected package python-setuptools.
   (Reading database ... 117857 files and directories currently installed.)
   Unpacking python-setuptools (from .../python-setuptools_0.6c9-0ubuntu4_all.deb) ...
   Setting up python-setuptools (0.6c9-0ubuntu4) ...

.. Once you have easy_install, installing CouchApp should be as easy as:

easy_installが使えるようになったら、CouchAppは簡単にインストールできます。

::

  sudo easy_install -U couchapp


.. You should see output like this:

次のような結果が返ってきたでしょうか。

::

   Searching for couchapp 
   Reading http://pypi.python.org/simple/couchapp/
   Reading http://github.com/couchapp/couchapp/tree/master
   Best match: Couchapp 0.3.31
   Downloading http://pypi.python.org/packages/source/C/Couchapp/Couchapp-0.3.31.tar.gz#md5=a346459155995942dea462e183f104f1
   Processing Couchapp-0.3.31.tar.gz
   Running Couchapp-0.3.31/setup.py -q bdist_egg --dist-dir /tmp/easy_install-Ey7eZR/Couchapp-0.3.31/egg-dist-tmp-QObSXT
   Adding Couchapp 0.3.31 to easy-install.pth file
   Installing couchapp script to /usr/local/bin 

   Installed /usr/local/lib/python2.6/dist-packages/Couchapp-0.3.31-py2.6.egg
   Processing dependencies for couchapp
   Finished processing dependencies for couchapp

.. Hopefull this worked and you are ready to start using CouchApp. If not, read on…The most common problem people experience installing CouchApp is with old versions of dependencies, especially easy_install itself. If you experienced an installation error, the best next step is to attempt to upgrade setuptools and then upgrade CouchApp, like this:

これで、CouchAppを使用する準備が整いました。うまくいかないようであれば、以降を読んでください。インストールではよくバージョンの互換性で問題になることがあります。とくにeasy_installのバージョンが古くないかを確認しましょう。次のように、`setuptoolsをアップグレードしてからCouchAppのインストールを試してみてください。

::

   sudo easy_install -U setuptools
   sudo easy_install -U couchapp

.. If you have other problems installing CouchApp, have a look at setuptools for Python’s easy install troubleshooting, or visit the CouchApp mailing list.

それでもうまくいかない場合は、 `setuptools <http://pypi.python.org/pypi/setuptools>`_ にある、easy_installについてのトラブルシューティングを確認しましょう。または、 `CouchAppのメーリングリスト <http://groups.google.com/group/couchapp>`_ を見て、問い合わせをするのもよいでしょう。

.. Using CouchApp

CouchAppを使う
~~~~~~~~~~~~~~~~~~~~

.. Installing CouchApp via easy_install should, as they say, be easy. Assuming all goes according to plan, it take care of any dependencies and puts the couchapp utility into your system’s PATH so you can immediately begin by running the help command:

CouchAppをEasy_installを介してインストールすることはとても簡単です。インストール時にPATHを通してあるため、コマンドを打てばすぐに使い始めることができます。ヘルプを表示させるには以下のように実行します。

::

   couchapp --help


.. You should see output like this:CouchApp help listing

CouchAppのヘルプメッセージが出力されたでしょうか。

::

   $ couchapp --help
   Usage: couchapp [options] cmd

   Options:
      --version show program's version number and exit
      -h, --help show this help message and exit
      -v print message to stdout
      -q don't print any message

   Generate a new CouchApp! (start here):
     couchapp generate «appname» [appdir]

   Pushes a CouchApp to CouchDB:
     couchapp push [options] [appdir] [appname] [dburl]

     --atomic store atomically the couchapp.
     --export Export the generated design doc to your console. If --output is specified, write to the file.
     --output=OUTPUT Combined with --export it allow you to save the generated design doc to the file.

   Clones/Pulls a CouchApp from a url (like http://host/db/_design/CA_name):
     couchapp clone/pull «dburl» [dir]

   Initialize CouchApp .couchapprc: 
     couchapp init [options] «appdir»

     --db=DB full url of default database

   Install a vendor:
     couchapp vendor install vendor_url [option][appdir]

     --scm=SCM scm used to install the vendor, by default git

.. We’ll be using the clone and push commands. Clone pulls an application from a running instance in the cloud, saving it as a directory structure on your filesystem. Push deploys a standalone CouchDB application from your filesystem to any CouchDB over which you have administrative control.

cloneとpushについて説明しましょう。cloneはCouchDBからアプリケーションを取得し、ローカルのファイルシステム上にあるディレクトリ構造で格納します。pushはCouchDBへアプリケーションを登録するときに使います。実行時にはCouchDBの管理者権限が必要になります。

.. Download the Sofa source code

Sofaのソースコードをダウンロードする
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. There are three ways to get the Sofa source code - each of them are equally valid, it’s just a matter of personal preference, and how you plan to use the code once you have it. The easiest way is to use CouchApp to clone it from a running instance. If you didn’t install CouchApp in the previous section, you can read the source code (but not install and run it) by downloading and extracting zip or tar file. If you are interested in hacking on Sofa, and would like to join the development community, the best way to get the source code is from the official Git repository. We’ll cover these three methods in turn.

Sofaのソースコードを取得する方法は三つあります。都合のよい方法で取得するのがよいでしょう。最も簡単なのはSofaが稼動しているCouchDB からCouchAppのcloneコマンドで取得する方法です。CouchAppをインストールしていない場合はzipまたはtar形式のファイルをダウンロードして解凍すればソースコードを読むことができます。Sofa自体をハックしたり、開発のコミュニティに参加したい場合はGitのリポジトリから取得するのが一番よいでしょう。この本の内容としては、どのやり方でも大丈夫です。

----

.. image:: images/11-01_missing.jpg
   :width: 500px
   :align: center

----

.. Bird Break: A happy bird to ease any install-induced frustration

*とまり木で休む小鳥: インストールのフラストレーションから解放された幸運な小鳥*

.. CouchApp Clone

CouchAppのクローンコマンドを実行する
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. One of the easiest ways to get the Sofa source code is by cloning directly from Chris’s blog using CouchApp’s clone command to download Sofa’s design document to a collection of files on your local harddrive. The clone command operates on a design document URL, which can be hosted in any CouchDB database accessible via HTTP. To clone Sofa from the version running Chris’s blog, run the following command:

最も簡単な方法はChrisさんのblogサイトに対してcloneコマンドを実行してSofaのデザインドキュメントを直接入手することです。 cloneコマンドはデザインドキュメントのURLを指定して実行します。HTTP経由でアクセスが可能なCouchDBに対して実行することができます。ChrisさんのブログからSofaのアプリケーションをcloneするには、次のコマンドを実行してみましょう。

::

   couchapp clone http://jchrisa.net/drl/_design/sofa

.. You should see this output:

次のような結果が表示されたでしょうか。


::

   [INFO] Cloning sofa to sofa...


.. Now that you’ve got Sofa on your local filesystem, you can skip to the bottom of the chapter to make a small local change and push it to your own CouchDB.

これでSofaがローカルのファイルシステム上に格納されたことになります。ローカルのCouchDB上で、Sofaを試してみたり、多少修正するぐらいであれば、以下の部分は読まなくても大丈夫です。

.. Zip and Tar files

Zip形式のファイルとTar形式のファイル
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. If you merely want to peruse the source code while reading along with the book, it is available as standard zip or tar downloads. To get the zip version access the following URL from your browser, which will redirect to the latest zip file of Sofa. If you prefer, a tar file is available as well.

ソースコードをこの本に沿って読んでいきたいのであれば、zip形式またはtar形式のファイルをダウンロードするのがよいかもしれません。zip形式のファイルを取得するには `このURLへ <http://github.com/jchris/couchapp/zipball/master>`_ アクセスしてください。tar形式のファイルは `こちらから <http://github.com/jchris/couchapp/tarball/master>`_ 入手することができます。

.. Join the Sofa Development Community on Github

GithubでSofaの開発コミュニティに参加する
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. The most up-to-date version of Sofa will always be available at its public code repository. If you are interested in staying up to date with development efforts, and contributing patches back to the source, the best way to do it is via Git and Github.

最新バージョンのSofaは一般公開されている `ソースコードのリポジトリ <http://github.com/jchris/sofa>`_ から入手することができます。もし、開発やパッチの提供に興味があるなら、GitとGithubを利用するのがよいでしょう。

.. Git is a form of distributed version control - it allows groups of developers to track and share changes to software. If you are familiar with Git, you’ll have no trouble using it to work on Sofa. If you’ve never used Git before, it has a bit of a learning curve, so depending on your tolerance for new software, you might want to save learning Git for another day - or you might want to dive in head first! For more information about Git, and how to install it, see the official Git home page. For other hints and help using Git, see the Github guides.To get Sofa (including all development history) using Git, run the following command.

Gitは分散バージョン管理ツールであり、開発者のグループがソフトウェアの変更を共有したり、追跡するのに役に立ちます。すでにGitに慣れている方にとってはSofaのコードを取り扱う点ことに何の問題もないでしょう。まだGitを使ったことがない方にとっては、少し敷居が高いかもしれません。Gitに関する詳しい情報やインストールの方法などは `Gitの公式サイト <http://git-scm.com/>`_ を見てください。また、Githubにある `ガイド <http://github.com/guides>`_ も参考になると思います。 Sofaのソースコード(開発履歴を含む)をGitから取得するには以下のようにコマンドを実行します。

::

   git clone git://github.com/jchris/sofa.git


.. If all goes well you should see output like this:

うまくいけば、以下のように表示されるでしょう。

::

   Initialized empty Git repository in /Users/me/sofa/.git/
   remote: Counting objects: 1103, done.
   remote: Compressing objects: 100% (663/663), done.
   remote: Total 1103 (delta 598), reused 658 (delta 360)
   Receiving objects: 100% (1103/1103), 142.48 KiB, done.
   Resolving deltas: 100% (598/598), done.

.. Now that you’ve got the source, lets take a quick tour.

これでソースコードを取得できました。ソースコードの構成について、簡単に説明します。

.. The Sofa Source Tree

Sofaのソースコードツリー
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. Once you’ve succeeded with any of these methods, you’ll have a copy of Sofa on your local disk. The following text is generated by running the tree command on the Sofa directory, to reveal the full set of files it contains. It is annotated inline to make it clear how various files and directories correspond to the Sofa design document.

以下のテキストデータはSofaのディレクトリ配下をtreeコマンドで表示させたものです。適宜、解説を入れていきます。

:: 

   sofa/
   |-- README.md
   |-- THANKS.txt

.. The source tree contains some files which aren’t necessary for the application - the README and THANKS files are among those.

ソースツリーには、実際のアプリケーションには必要のないファイルも含まれています。READMEやTHANKSといった類です。

::

   |-- _attachments
   | |-- LICENSE.txt
   | |-- account.html
   | |-- blog.js
   | |-- jquery.scrollTo.js
   | |-- md5.js
   | |-- screen.css
   | |-- showdown-licenese.txt
   | |--showdown.js
   | |-- tests.js
   | `-- textile.js

.. The _attachments directory contains files which are saved to the Sofa design document as binary attachments. CouchDB serves attachments directly (instead of including them in a JSON wrapper) so this is where we store JavaScript, CSS, and HTML files that the browser will access directly.

_attachmentsディレクトリはバイナリデータの添付ファイルとして Sofaのデザインドキュメントへ格納されます。CouchDBは添付ファイルをそのままの状態で管理してくれます(JSONラッパーを介さない形で)。そのため、JavaScript,CSS,HTMLファイルに対して直接ブラウザでアクセスすることができます。

::

   |-- blog.json

.. Making your first edit to the Sofa source code will show you how easy it is to modify the application.

.. Note::
   初めてSofaのソースコードを編集するなら、ここから始めるのがお勧めです。
   
   

.. The blog.json file contains JSON used to configure individual installations of Sofa. Currently it sets one value, the title of the blog. You should open this file now and personalize the title field - you probably don’t want to name your blog "Daytime Running Lights", now’s your chance to come up with something more fun!

blog.json ファイルはSofaの設定を個別に変更するためのファイルです。最初はブログのタイトルのみが設定値として登録されています。早速、タイトルを変更してみてください。おそらく、"Daytime Running Lights"というタイトルをお好きな文字列に変更してみましょう。

.. You could add other blog configuration to this file, maybe things like how many posts to show per page, and a URL to an about page for the author. Working changes like these into the application will be easy once you’ve walked through the following chapters.

このファイルには他にも様々な設定を書いておくことができます。ひとつのページに対する投稿数を表示させるようにしたり、ブログの著者のプロフィールに関するURLを表示させるといったような設定です。この章を読み進めていけば、簡単にこうした設定変更ができるようになります。

::

   |-- couchapp.json

.. We’ll see later that couchapp outputs a link to Sofa’s home page when couchapp push is run. The way this works is pretty simple - CouchApp looks for an JSON field on the design document, at the address design_doc.couchapp.index, if it finds it, it appends the value to the location of the design doc itself to build the URL. If there is no CouchApp index specified, but the design document has an attachment called index.html, then it is considered the index page. In Sofa’s case we use the index value to point to a list of the most recent posts.

couchappのpushコマンドを実行すると、Sofaのトップページへのリンクが出力されます。この仕組みはシンプルなものです。couchappはdesign_doc.couchapp.index というJSONの項目を探し、見つかった場合はデザインドキュメントのURLをサイトのロケーションとして代入します。couchappのインデックスが特定できない場合は、attachmentにindex.htmlがあれば、そこをトップページとみなします。Sofaの場合は最近の投稿を一覧で表示するlist関数へのURLをトップページに指定しています。

::

   |-- helpers
   | `-- md5.js

.. The helpers directory here is just an arbitrary choice - CouchApp will push any files and folders to the design document - in this case the source code to md5.js is JSON encoded and stored on thedesign_document.helpers.md5 element.

helpersディレクトリはひとつの例として選択しました。couchappはpushコマンドを実行すると複数のファイルやフォルダをデザインドキュメントとして登録します。上記の場合はmd5.jsというファイルがJSON形式にエンコードされ、design_document.helpers.md5属性として登録されます。

::

  |-- lists
  | `-- index.js

.. The lists directory contains a JavaScript function that will be executed by CouchDB to render view rows as Sofa’s HTML and Atom indexes. You could add new list functions by creating new files within this directory. Lists are covered in depth in the Viewing Lists of Blog Posts Chapter.

listsディレクトリにはJavaScriptの関数を格納します。上記のファイル内にある関数はビューのデータをレンダリングしてSofaのHTMLとAtomフィードとして出力するためのものです。listsディレクトリ内に新しいファイルを配置して新しくlist関数を作ることもできます。list関数に関する詳細は"Viewing Lists of Blog Posts"の章で取り扱います。

::

  |-- shows
  |   |-- edit.js
  |   `-- post.js

.. The shows directory holds the functions CouchDB uses to generate HTML views of blog posts. There are two views, one for reading posts, and the other for editing. We’ll look at these functions in the next few chapters.

showsディレクトリにはブログの記事をHTML形式でレンダリングするための関数を登録しています。例ではブログの記事を読む場合と編集する場合とで二つの関数を登録しています。show関すについては次の章で詳しく取り扱います。

::

   |-- templates
   | |-- edit.html
   | |-- index
   | | |-- head.html
   | | |-- row.html
   | | `-- tail.html
   | `-- post.html

.. The templates directory is like the helpers directory, and unlike the lists, shows, or views directory, in that the code stored this is not directly executed on CouchDB’s server side. Instead, the templates are included into the body of the list and show functions using macros run by CouchApp when pushing code to the server. These CouchApp macros are covered later in this chapter. The key point is that the templates name could be anything - it is not a special member of the design document, just a convenient place to store and edit our template files.

templateディレクトリは先ほどのhelpersディレクトリに似ています。 lists,shows,viewsディレクトリとは違って、CouchDBのサーバー側では実行されないコードが格納されます。その代わり、 couchappのmacroを使うとlist関数やshow関数内にtemplateの内容を展開させることができます。pushコマンドを実行したタイミングで実行されます。templateディレクトリの名前は何でも結構です。デザインドキュメントを管理するための特別なディレクトリには直接含まれないためです。

::

   |-- validate_doc_update.js

.. This file corresponds to the JavaScript validation function used by Sofa to ensure that only the blog owner can create new posts, as well as ensuring the comments are well formed. Sofa’s validation function is covered in detail in Storing Documents.

このファイルにはバリデーション関数を書いておきます。Sofaではバリデーション関数を用いてブログのオーナーのみが記事を書くことができるといった制約を実装しています。Sofaのバリデーション関数については次の章でさらに詳しく取り扱います。

::

   |-- vendor
   | `-- couchapp
   | |-- README.md
   | |-- _attachments
   | | `-- jquery.couchapp.js
   | |-- couchapp.js
   | |-- date.js
   | |-- path.js
   | `-- template.js

.. The vendor directory holds code that is managed independently of the Sofa application itself. In Sofa’s case the only vendor package used is couchapp, which contains JavaScript code that knows how to do things like link between list and show URLs and render templates.

vendorディレクトリではSofaが取り込んでいるプラグインのように使えるvendorパッケージのソースコードを格納しています。 Sofaの場合、vendorパッケージはcouchappのみです。couchappパッケージによってlist関数とshow関数などの効率のよい管理や、templateの展開する機能が提供されています。

.. During couchapp push files within a vendor/**/_attachments/* path are pushed as design document attachments. In this case jquery.couchapp.js will be pushed to an attachment calledcouchapp/jquery.couchapp.js (so that multiple vendor packages can have the same attachment names without worry of collisions.)

vendor内にある_attachments配下のファイルはデザインドキュメント内のattachmentsとして、pushコマンド実行時に格納されます。この場合はjquery.couchapp.jsというファイルがcalledcouchapp/jquery.couchapp.jsとして登録されます(これにより、複数のvendorパッケージが同じ添付ファイルを持っていたとしても競合することがありません)。

::

  `-- views
   |-- comments
   | |-- map.js
   | `-- reduce.js
   |-- recent-posts
   | `-- map.js
   `-- tags
   |-- map.js
   `-- reduce.js

.. The views directory holds Map Reduce view definitions, with each view represented as a directory, holding files corresponding to map and reduce functions.

viewsディレクトリではMap&Reduceによるviewの定義を管理しています。各viewはそれぞれひとつのディレクトリ単位で管理され、ディレクトリの中にmap関数とreduce関数のファイルを配置するようにします。

.. Deploying Sofa

Sofaをデプロイする
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. The source code is safely on your hard drive, and you’ve even been able to make minor edits to theblog.json file. Now it’s time to deploy the blog to a local CouchDB. The push command is very simple, and should work the first time, but there are two other steps involved in setting up an admin account on your CouchDB, and for your CouchApp deployments. By the end of this chapter you’ll have your own running copy of Sofa.

blog.json ファイルに任意の変更を加えたら、ローカルのCouchDBへデプロイしてみましょう。pushコマンドはとてもシンプルで、簡単に動作しますが、 CouchDBの管理者権限が必要になります。この章が終わるころにはSofaをコピーしたものをローカルの環境で動かせるようになるでしょう。

.. Pushing Sofa to Your CouchDB

SofaをCouchDBへpushする
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. Anytime you make edits to the on-disk version of Sofa, and want to see them in your browser, run the following command:

ローカルのソースコードを編集して、ブラウザで変更内容を確認したくなったら次のようにコマンドを打ってみましょう。

::

   couchapp push . sofa


.. This deploys the Sofa source code into CouchDB. You should see output like this:

これでCouchDBへSofaのソースコードがデプロイされたことになります。次のようなメッセージが出力されているでしょうか。

::

   [INFO] Pushing CouchApp in /Users/jchris/sofa to design doc:
   http://127.0.0.1:5984/sofa/_design/sofa
   [INFO] Visit your CouchApp here:
   http://127.0.0.1:5984/sofa/_design/sofa/_list/index/recent-posts?descending=true&limit=5

.. If you get an error, make sure your target CouchDB instance is running by making a simple HTTP request to it:

エラーが出た場合は、pushコマンドの対象になっているCouchDBのインスタンスがHTTPリクエストに応答するか以下のように確認してみましょう。

::

   curl http://127.0.0.1:5984

.. The response should look a lot like:

以下のように返ってくれば、デプロイの対象にしているインスタンスには問題ありません。

::

   {"couchdb":"Welcome","version":"0.10.0"}

.. If CouchDB is not running yet, go back to the Getting Started chapter and follow the hello world instructions there.

CouchDBがまだ稼動していなかったら、Getting Startedの章に戻って導入方法を確認してみてください。

.. Visit the Application

Webアプリケーションにアクセスしてみる
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

If CouchDB was running, then couchapp push should have directed you to visit the application’s index URL. Visting the URL should show you something like this:

pushコマンドを実行したときに、 `アプリケーションのURL <http://127.0.0.1:5984/sofa/_design/sofa/_list/index/recent-posts?descending=true&limit=5>`_ が出力されたはずです。ブラウザでアクセスすると、以下のような画面が出力されると思います。

----

.. image:: images/11-02_empty-index.png
   :width: 500px
   :align: center

----

.. Figure: Empty index page

*画像: まだ記事がないトップページ*

.. We’re not done yet - there are a couple of steps remaining before you’ve got a fully functional Sofa instance.

Sofaを使いこなせるようになるまで、あともう少しです。

.. Setup Your Admin Account

管理者権限を設定する
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. Sofa is a single-user application. You, the author, are the administrator and the only one who can add and edit posts. To make sure no one else goes in and messes with your writing, you must create an administrator account in CouchDB. This is a straightforward task. Find your local.ini file and open it in your text editor. (By default it’s stored at /usr/local/etc/couchdb/local.ini) If you haven’t already, uncomment the [admins] section at the end of the file. Next, add a line right below the[admins] section with your preferred username and password.

Sofaはユーザー1人向けのアプリケーションです。ブログの著者としてのユーザーのみが記事を投稿し、編集することができます。他の誰かによる改ざんなどを防ぐために、CouchDB内で管理者アカウントを用意しておく必要があります。local.iniファイルを探し、テキストエディタで開いてみてください(/usr/local/etc/couchdb/local.ini がデフォルトの場所になります)。[admins]セクションの脇にあるコメント記号を外してください。その後、[admins]セクションの配下にユーザー名とパスワードを記述します。

::

   [admins]
   jchris = secretpass

.. Now that you’ve edited your local.ini configuration file, you need to restart CouchDB for changes to take effect. Depending on how you started CouchDB, there are different methods of restarting it. If you started in a console, then hitting control-c, and rerunning the same command you used to start it is the simplest way.If you don’t like your passwords lying around in plain-text files, don’t worry. When CouchDB starts up and reads this file, it takes your password and changes it to a secure hash, like this:

local.iniの変更を適用するためにはCouchDBを再起動する必要があります。再起動の方法はいくつかります。コンソールから CouchDBを起動している場合は、Ctrl + Cを押して一旦停止した後に、再度CouchDBを起動します。先ほどのlocal.iniファイルではパスワードを平文で管理しているのでしょうか。心配はいりません。CouchDBが変更後のlocal.iniファイルを読み込んだ時点で、パスワードは以下のようにハッシュ値に変換されるようになっています。

::

   [admins]
   jchris = -hashed-207b1b4f8434dc604206c2c0c2aa3aae61568d6c,964 \ 06178007181395cb72cb4e8f2e66e

.. CouchDB will now ask you for your credentials when you try to create databases or change documents; exactly the things you want to keep to yourself.

これで、データベースを作成したり、ドキュメントを変更しようとすると認証が要求されるようになります。

.. Deploying to a Secure CouchDB

セキュアなCouchDBへデプロイする
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. Now that we’ve set up admin credentials, we’ll need to supply them on the command line when running couchapp push. Let’s try it:

管理者権限を設定した後のpushコマンドは以下のように実行します。

::

   couchapp push . http://jchris:secretpass@localhost:5984/sofa

.. Make sure to replace jchris and secretpass with your actual values or you will get a permission denied error. If all works according to plan everything is set up in CouchDB and you should be able to start using your blog.

jchrisという名前とパスワードは各自変更してご利用ください。おそらく、パーミッションエラーになるでしょう。これまでの設定がうまくいけば、ブログアプリケーションを利用する準備が全て整ったことになります。

.. At this point we are technically ready to move on, but you’ll be much happier if you make use of the.couchapprc file as documented in the next section.

あとは、.couchapprcの使い方を覚えればさらに快適に開発ができるようになります。次の節で紹介します。

.. Configuring CouchApp with .couchapprc

.couchapprcでCouchAppの設定を管理する
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. If you don’t want to have to put the full URL (potentially including authentication parameters) to your database onto the command line each time you push, you can use the .couchapprc file to store deployment settings. The contents of this file are not pushed along with the rest of the app, so it can be a safe place to keep credentials for uploading your app to secure servers.The .couchapprc file lives in the source directory of your application, so you should look to see if it is at /path/to/the/directory/of/sofa/.couchapprc (or create it there if it is missing). Dotfiles (files with names that start with a period) are left out of most directory listings. Use whatever tricks your OS has to "show hidden files" - the simplest one in a standard command shell is to list the directory using ls -a which will show all hidden files as well as normal files.

pushコマンドを実行する度に、URLを全て記入するのが煩わしい(特に認証機能を有効にしてから)と思っている方は.couchapprcファイルに設定を書き込んでおくとよいでしょう。このファイルはpushコマンドの対象になりません。そのため、認証情報等を書いておいても問題ありません。.couchapprcは各アプリケーションのディレクトリ内に存在しています。sofaなら、/path/to/the/directory/of/sofa/.couchapprc といった具合です。ドットで始まるファイルは隠しファイルの扱いになり、通常は表示されません。隠しファイルも表示する設定にすれば確認することができます。たとえば、-aオプションを追加してlsコマンドを実行してみます。

.. An Example .couchapprc

.couchapprcの例

::

   {
     "env": {
       "default": { "db": "http://jchris:secretpass@localhost:5984/sofa" },
       "staging": { "db": "http://jchris:secretpass@jchrisa.net:5984/sofa-staging" },
       "drl": { "db":"http://jchris:secretpass@jchrisa.net/drl" } 
     }
   }

.. With this file set up, you can push your CouchApp with the command couchapp push, which will push the application to the "default" database. CouchApp also supports alternate environments. To push your application to a development database, you could use couchapp push dev. In our experience, taking the time to setup a good .couchapprc is always worth it. Another benefit is that it keeps your passwords off the screen when you are working.

準備ができたら、pushコマンドを実行しましょう。何も指定しない場合は"default"に書いてあるデータベースに対してpushコマンドを実行します。CouchAppは複数の開発環境を容易に切り替えることができます。開発用のデータベースにアプリケーションをpushする場合は couchapp push dev と実行します。経験上、.couchapprcを使って設定を保存しておくことをお勧めします。パスワードを毎回、画面に表示させなくてもよくなります。

`Copyright 2009 <http://creativecommons.org/licenses/by/3.0/>`_ , `J. Chris Anderson <http://jchris.mfdz.com/>`_ , `Jan Lehnardt <http://jan.prima.de/>`_ & `Noah Slater <http://tumbolia.org/nslater>`_ 

Translated by z.ohnami

