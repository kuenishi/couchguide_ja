.. -*- mode: rst -*-

.. All these works are translation of "CouchDB: The Definitive Guide".
.. highlight:: javascript

.. 14. Scaling Basics
   http://books.couchdb.org/relax/reference/scaling-basics

15. スケーリングの基本
-----------------------------

.. Scaling is an overloaded term. Finding a discrete definition is tricky. 
   Everyone and her grandmother have their own idea what scaling means. 
   Most definitions are valid, but they can be contradicting. 
   To make things even worse, there are a lot of misconceptions about scaling. 
   To find out what it is, one needs a scalpel to find out the important bits.

「スケーリング」は、使い古された用語です。独立した定義はどうしてもトリッキーなものになってしまいます。人によって、スケーリングの意味は随分変わります（おばあちゃんだって）。そのほとんどは間違ってはいないのですが、それぞれが矛盾する場合もあります。さらに困ったことに、スケーリングについて誤解している人も多いです。スケーリングとは何かを理解するためには、核心に至るためのメスが必要です。

.. First, scaling doesn’t refer to a specific technique or technology, scaling, or scalable, is an attribute of a specific architecture. What is being scaled varies for nearly each project.

まず、「スケーリング」とは、ある特定のテクニックや技術を表すものではなく、「スケーリング」「スケーラブル」とはただのアーキテクチャの特徴に過ぎません。何がスケールするかは、プロジェクトによってまちまちです。

.. Scaling is specialization.— Joe Stump, Lead Architect of digg.com

::

   スケールさせるとは、何かに特化するということだ。
                        -- Joe Stump, digg.comリードアーキテクト

.. Joe’s quote is the one that we find is the most accurate description of scaling. It is also wishy-washy; but that is the nature of scaling. An example: A website like facebook.com with a whole lot of users and data associated with these users and with more and more users coming in everyday might want to scale over user-data that typically lives in a database. In contrast flickr.com is at it’s core like Facebook with users and data for users, but in flickr’s case, the data that grows fastet is images uploaded by users. These images do not necessarily live in a database so scaling image storage is flickr’s path to growth. [fact check and/or find better example].

この引用は、スケーリングについてのほぼ全てを表しています。\
なんとも薄いですが、これがスケーリングというものです。\
例えば、facebook.comのように多くのユーザデータ、ユーザ関連データあって、日夜ユーザが増加しているようなサイトでは、データベースにあるユーザデータについて、システムをスケールさせたいと思っていることでしょう。対照的に、flickr.comは、Facebookのようなユーザデータもありますが、この場合はデータはユーザのアップロードに伴って増加します。これらの画像データは必ずしもデータベースにあるわけではありませんが、画像データのストレージをスケールさせるのがflickrの発展の近道でしょう。

.. Note: It is common to think of scaling as scaling out. This is shortsighted. Scaling can also mean scaling in - that is, being able to use fewer computers when demand declines. More on that later.

.. Note::
   スケーリングはスケールアウトと混同されがちですが、これは少し安直です。スケーリングには、スケールインの含まれます - つまり、ニーズが下がるに従ってシステムを縮小させることもスケーリングのうちです。後でまたこの話をします。

.. These are just two services. There are a lot more and every one has different things they want to scale. CouchDB is a database; we are not going to cover every aspect of scaling any system. We concentrate on the bits that are interesting to you, the CouchDB user. We identified three general properties that you can scale with CouchDB:

これらは、ふたつのサービスに過ぎません。彼らがスケールさせたいものは他にも沢山あります。CouchDB はデータベースですので、全てのシステムのあらゆる側面をカバーしようとしているわけではありません。CouchDBユーザのあなたが興味を持っていることにだけ注目します。ここでは、CouchDBであなたがスケールさせることができる、最も一般的な３個の特性を示します。

* 読み込み性能 / Read requests
* 書き込み性能 / Write requests
* データ / Data

.. Scaling Read Requests

読み込み性能をスケールさせる
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. A read request is retrieves a piece of information from the database. 
   It passes the following stations within CouchDB: 
   The HTTP server module needs to accept the request. For that, it opens a socket to send data over. 
   The next station is the HTTP request handle module that analyzes the request and directs it to the appropriate sub-module in CouchDB. For single documents, the request then gets passed to the database module where the data for the document is looked up on the filesystem and returned all the way up again.

読み込み要求は、データベースからのデータ断片の要約です。CouchDBの内部で、次のような駅を通過します：
HTTPサーバモジュールが、要求を accept します。つまりデータを送信するためのソケットを確保します。次の駅はHTTPリクエストを処理するモジュールで、リクエストを解析してCouchDB内の適切なサブモジュールにデータを渡します。単一ドキュメントの場合は、リクエストはデータベースモジュールに渡されて、ファイルシステムに対してドキュメントのデータ検索を行い、もと来た道をまた帰っていきます。

.. All this takes processing time and enough sockets (or file descriptors) must be available.
   The storage backend of the server must be able to fulfill all read requests. 
   There are a few more things that can limit a system to accept more read requests;
   the basic point here is that a single server can only process so many concurrent requests.
   If your applications generates more requests you need to set up a second server that your application can read from.

これは結構時間のかかる処理なので、十分なソケットが利用可能でなければなりません。サーバのストレージは全ての読み込み要求を処理できなければなりません。他にも、いくつか阻害要因はありますが、基本的にはひとつのサーバで多くの読み込み要求を処理したい場合には、それらの要求がコンカレントなものでなければなりません。もしも、あなたのアプリケーションがさらに多くのリクエストを出してくるようであれば、読み込み用に二台目のサーバを用意するべきです。

.. The nice thing about read requests is that they can be cached. 
   Often-used items can be held in memory and can be returned at a much higher level than the one that is your bottleneck. Requests that can use this cache, don’t ever hit your database and are thus virtually toll-free. The Load Balancing chapter explains this scenario.

読み込み要求だけなら、キャッシュを使うのがイケてるでしょう。よく使われるデータはメモリ上に保持して、ボトルネックがある場合よりもずっと速く返すことができるでしょう。キャッシュを読むリクエストは絶対にデータベースを更新してはいけませんが、これ以外に制約はありません。ロードバランシングの章でこのシナリオについて詳しく説明します。

.. Scaling Write Requests

書き込み性能をスケールさせる
~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. A write requests is like a read request, only a little worse.
   It not only reads a piece of data from disk, it writes it back after modifying it. Remember the nice thing about reads being cacheable. Writes: Not so much. A cache must be notified when a write changes data or clients must be told to not use the cache. If you have multiple servers for scaling reads, a write must occur on all servers. In any case, you need to work harder with a write. The Clustering chaptercovers methods for scaling write requests across servers.

書き込み要求は読み込み要求の場合と比べて、少し大変なだけでそう変わりません。書き込みでは、ディスクからデータ断片を読み出すだけでなく、データを変更した後にディスクに書き戻します。読み込みはキャッシュで済ませることができましたが、書き込みはそうはいきません。書き込みによってデータが変更されたり、クライアントがキャッシュを禁止したら、キャッシュは更新されなければなりません。読み込み性能をスケールさせるために複数サーバを使っている場合、書き込みは全サーバに対して発生してしまいます。どんな場合であれ、書き込みは読み込みよりも大変です。クラスタリングの章では、複数サーバ間での書き込み性能をスケールさせるための方法を示します。

.. Scaling Data

データ量をスケールさせる
~~~~~~~~~~~~~~~~~~~~~~~~

.. The third way of scaling is scaling data. Todays hard drives are cheap and have a lot of capacity, and it only gets better in the future, but there is only so much data a single server can make sensible use of. It must maintain one more indexes to the data which uses disk space again. Creating backups will take longer and other maintenance tasks become a pain.

スケーリングの三番目のパターンは、データ量です。今日ではハードディスクは安価で大容量、しかも増える一方。とはいえ、一台のサーバに格納できる量は限られているし、他にも、データとしては重複しているインデックスデータも入れなければなりません。バックアップを作るのにもより長い時間がかかりますし、他のメンテナンス作業も苦痛になります。

.. The solution is to chop the data into manageable chunks and put each chunk on a separate server. All servers with a chunk now form a cluster that holds all your data. The Clustering chapter takes a look at creating and using these clusters.

そこで、管理可能なチャンクにデータを分割して、チャンクを別々のサーバに置きます。\
チャンクを持った全てのサーバがクラスタを構成し、あなたのデータ全てを管理します。\
クラスタリングの章では、クラスタの作成と使用について概説します。

.. While we are taking separate looks at scaling of reads, writes, and data, these rarely occur isolated. Decisions to scale one will affect the others. We will describe individual as well as combined solutions in the following chapters.

読み込み性能、書き込み性能、データ量について個々に見てきましたが、これらは簡単に分離できるものではありません。どれかをスケールさせれば、他のどれかに影響します。最初に個々に説明しますが、後の章でそれらを組み合わせた場合について説明します。

.. Basics First

最初は基本から
~~~~~~~~~~~~~~~

.. Replication is the basis for all of the three scaling methods. Before we go scaling, the Replication chapter will get you familiar with CouchDB’s excellent replication feature.

レプリケーションがこれらの三通りのスケーリング手法の基本になります。スケーリングの話に進む前に、レプリケーションの章で、CouchDBの素敵なレプリケーション特性について詳しくなっておくべきです。


`Copyright 2009 <http://creativecommons.org/licenses/by/3.0/>`_ , J. Chris Anderson, Jan Lehnardt & Noah Slater

Translated by UENISHI Kota
